﻿using UnityEngine;
using System.Collections;

public class BuilderSnap : MonoBehaviour
{

    public string partnerTag;
    public float closeVPDist = 0.05f;
    public float farVPDist = 1;
    public float moveSpeed = 40.0f;
    public float rotateSpeed = 8.0f;

    private Vector3 screenPoint;
    private Vector3 offset;
    Color color = new Color(1, 0, 0);

    float dist = Mathf.Infinity;
    Color normalColor;
    GameObject partnerGO;

    //Gets and stores original material color 
    void Start()
    {
        //normalColor = GetComponent<Renderer>().material.color;
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("BotTag");
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                partnerGO = go;
                distance = curDistance;
            }
        }

        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
        Vector3 partnerPos = Camera.main.WorldToViewportPoint(partnerGO.transform.position);
        Vector3 myPos = Camera.main.WorldToViewportPoint(transform.position);
        dist = Vector2.Distance(partnerPos, myPos);
        //GetComponent<Renderer>().material.color = (dist < closeVPDist) ? color : normalColor;

        Cursor.visible = true;
        if (dist < closeVPDist)
        {
            transform.SetParent(partnerGO.transform);

            StartCoroutine(InstallPart());
            //GetComponent<Renderer>().material.color = normalColor;
        }
        if (dist > farVPDist)
        {
            transform.SetParent(null);
        }
    }

    /*
    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100.0f) && hit.transform.gameObject != null && hit.transform.tag == "BotPart")
            {
                // here you need to insert a check if the object is really a tree
                // for example by tagging all trees with "Tree" and checking hit.transform.tag
                //transform.DetachChildren();
                GameObject.Destroy(hit.transform.gameObject);
            }
        }
    }


    //Get current position of mouse and set offset 
    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        //Cursor.visible = false;
    }

    //Finds the closest snap point within list of snap points with BotTag
    void OnMouseDrag()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("BotTag");
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                partnerGO = go;
                distance = curDistance;
            }
        }

        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
        Vector3 partnerPos = Camera.main.WorldToViewportPoint(partnerGO.transform.position);
        Vector3 myPos = Camera.main.WorldToViewportPoint(transform.position);
        dist = Vector2.Distance(partnerPos, myPos);
        GetComponent<Renderer>().material.color = (dist < closeVPDist) ? color : normalColor;
    }

    //If closest snap point is within minimum snap range of dragged object set object as child of snap point and start InstallPart coroutine
    //If dragged part is outside of maximum snap range unparent dragged object from snap point
    void OnMouseUp()
    {
        Cursor.visible = true;
        if (dist < closeVPDist)
        {
            transform.SetParent(partnerGO.transform);

            StartCoroutine(InstallPart());
            GetComponent<Renderer>().material.color = normalColor;
        }
        if (dist > farVPDist)
        {
            transform.SetParent(null);
        }
    }*/

    //Set position and rotation of dragged object to that of snap point
    IEnumerator InstallPart()
    {
        while (transform.localPosition != Vector3.zero || transform.localRotation != Quaternion.identity)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero, Time.deltaTime * moveSpeed);
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.identity, rotateSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

}
