﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInstantiate : InventoryUI
{
    public GameObject botbodyGO;
    public GameObject botwingGO;
    public GameObject botpogoGO;

    private GameObject botpartGO = null;

    // Start is called before the first frame update
    void Start()
    {
        botpartGO = botbodyGO;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100.0f) && botbodyGO != null && hit.transform.gameObject != null)
            {
                if (botpartGO == botbodyGO)
                {
                    bodyCount -= 1;
                }
                else if (botpartGO == botwingGO)
                {
                    wingCount -= 1;
                }
                else if (botpartGO == botpogoGO)
                {
                    pogoCount -= 1;
                }
                else
                {
                }
                // here you need to insert a check if the object is really a tree
                // for example by tagging all trees with "Tree" and checking hit.transform.tag
                //Transform.DetachChildren
                Instantiate(botpartGO, hit.point, Quaternion.identity);
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100.0f) && hit.transform.gameObject != null && hit.transform.tag == "BotPart")
            {
                // here you need to insert a check if the object is really a tree
                // for example by tagging all trees with "Tree" and checking hit.transform.tag
                //transform.DetachChildren();
                GameObject.Destroy(hit.transform.gameObject);
            }
        }
    }

    public void BotBaseSelect()
    {
        botpartGO = botbodyGO;
    }

    public void BotWingSelect()
    {
        botpartGO = botwingGO;
    }

    public void BotPogoSelect()
    {
        botpartGO = botpogoGO;
    }
}