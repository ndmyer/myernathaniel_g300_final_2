﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuilderCamRotate : MonoBehaviour
{

    public float speed;

    public Transform target;

    public Vector3 offset = new Vector3(0, 0, 1);

    public float minZoom = 5f;
    public float maxZoom = 15f;
    private float zoomSpeed = 5.0f;
    float angle;
    float currentZoom;

    // Start is called before the first frame update
    void Start()
    {
        float angle = Mathf.Atan(target.position.z - transform.position.z); 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            transform.RotateAround(target.position, transform.right, -Input.GetAxis("Mouse Y") * speed);
            transform.RotateAround(target.position, transform.up, Input.GetAxis("Mouse X") * speed);
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, angle);
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        transform.position += this.transform.forward * scroll * zoomSpeed;
        currentZoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
        currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);
    }

    void LateUpdate()
    {
        //transform.position = target.position - offset * currentZoom;
    }
}
