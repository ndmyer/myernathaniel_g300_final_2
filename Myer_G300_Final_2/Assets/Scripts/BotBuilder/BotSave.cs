﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotSave : MonoBehaviour
{
    public GameObject botGO;

    private void Awake()
    {
        if (ES3.KeyExists("BotBodyBase"))
            ES3.Load<GameObject>("BotBodyBase");
    }

    /*
    void Update()
    {
        if (Input.GetKeyDown("s"))
        {
            ES3.Save<GameObject>("BotBodyBase", botGO);
            Debug.Log(Application.persistentDataPath);
        }
    }
    */
}
