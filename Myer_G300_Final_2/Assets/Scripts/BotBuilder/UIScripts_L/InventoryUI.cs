﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InventoryUI : BotBuilderScrapUI
{
    private Vector3 screenPoint;

    [HideInInspector]
    static public int bodyCount;
    [HideInInspector]
    static public int wingCount;
    [HideInInspector]
    static public int pogoCount;

    public Text bodycountText;
    public Text wingcountText;
    public Text pogocountText;

    //Sets to current scene
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    //Loads count value on current scene load
    //Sets count ui to count value
    void OnSceneLoaded(Scene BotBuilderScene, LoadSceneMode single)
    {
        bodyCount = ES3.Load<int>("BotBodyCount");
        wingCount = ES3.Load<int>("BotWingCount");
        pogoCount = ES3.Load<int>("BotPogoCount");
        bodycountText.text = bodyCount.ToString("0");
        wingcountText.text = wingCount.ToString("0");
        pogocountText.text = pogoCount.ToString("0");
    }

    //If scrap is equal or more than cost inceases item count 
    //decreases scrap by cost
    //and updates count ui
    public void BotBaseBuy()
    {
        if(scrap >= 100)
        {
            scrap -= 100;
            bodyCount += 1;
            bodycountText.text = bodyCount.ToString("0");

        }
    } 

    public void BotWingBuy()
    {
        if (scrap >= 100)
        {
            scrap -= 100;
            wingCount += 1;
            wingcountText.text = wingCount.ToString("0");

        }
    }

    public void BotPogoBuy()
    {
        if (scrap >= 100)
        {
            scrap -= 100;
            pogoCount += 1;
            pogocountText.text = pogoCount.ToString("0");

        }
    }

    //Saves count value on current scene quit
    void OnDisable()
    {
        ES3.Save<int>("BotBodyCount", bodyCount);
        ES3.Save<int>("BotWingCount", wingCount);
        ES3.Save<int>("BotPogoCount", pogoCount);
    }
}
