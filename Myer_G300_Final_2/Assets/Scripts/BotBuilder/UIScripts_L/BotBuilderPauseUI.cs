﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BotBuilderPauseUI : MonoBehaviour
{
    public static bool PauseGame = false;

    [HideInInspector]
    public GameObject pauseMenuUI;
    [HideInInspector]
    public GameObject botGO;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PauseGame)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        PauseGame = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        PauseGame = true;
    }

    public void LoadLaunch()
    {
        //Debug.Log("load botbuilder");
        ES3.Save<GameObject>("BotBodyBase", botGO);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene("LaunchScene");
    }

    public void QuitGame()
    {
        //Debug.Log("quit game");
        ES3.Save<GameObject>("BotBodyBase", botGO);
        Application.Quit();
    }
}
