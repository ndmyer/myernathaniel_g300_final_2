﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BotBuilderScrapUI : MonoBehaviour
{ 
    [HideInInspector]
    public static int scrap;

    public Text scrapText;

    //Sets to current scene
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    //Loads scrap value on current scene load
    void OnSceneLoaded(Scene BotBuilderScene, LoadSceneMode single)
    {
        scrap = ES3.Load<int>("scrap");
    }

    //Sets onscreen scrap ui to current scrap value
    void Update()
    {
        //scrap = gameObject.GetComponent<BotBuilderScrapUI>().scrap;
        scrapText.text = "Scrap: " + scrap.ToString("0");
    }

    //Saves scrap value on current scene quit
    void OnDisable()
    {
        ES3.Save<int>("scrap", scrap);
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
