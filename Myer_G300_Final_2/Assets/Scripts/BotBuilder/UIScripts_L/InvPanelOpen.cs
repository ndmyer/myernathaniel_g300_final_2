﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvPanelOpen : MonoBehaviour
{
    public static bool Open = false;

    public GameObject invPanel;

    //When tab is pressed switches bool which plays an animation dependent on state
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (invPanel != null)
            {
                Animator animator = invPanel.GetComponent<Animator>();
                if (animator != null)
                {
                    bool isOpen = animator.GetBool("Open");

                    animator.SetBool("Open", !isOpen);
                }
            }
        }
    }
}
