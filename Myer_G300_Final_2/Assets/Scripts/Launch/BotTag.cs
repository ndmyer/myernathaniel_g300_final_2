﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotTag : MonoBehaviour
{
    private int children;
    private int mass;
    private Rigidbody rb;

    private void Start()
    {
        children = transform.childCount;
        mass = 1 + children;
        rb = GetComponent<Rigidbody>();
        rb.mass = mass;
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Debug.Log("collided with " + collision.gameObject.name);
    }

}
