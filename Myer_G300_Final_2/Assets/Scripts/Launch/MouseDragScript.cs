﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDragScript : MonoBehaviour
{
    private Vector3 mOffset;
    private Vector3 gameObjectStart;

    private float mZCoord;

    // sets gameObjectStart variablle to game objects starting position
    private void Start()
    {
        gameObjectStart = gameObject.transform.position;
    }

    private void OnMouseDown()
    {
        //Store offset = GameObject world pos - MouseDragScript world pos
        mOffset = gameObject.transform.position - GetMouseWorldPos();
    }

   // private void OnMouseUp()
   //{
   //     gameObject.transform.position = gameObjectStart;
   // }

    private Vector3 GetMouseWorldPos()
    {
        mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;

        //Mouse position in pixel coordinates
        Vector3 mousePoint = Input.mousePosition;

        //z coordinate of selected game object
        mousePoint.z = mZCoord;

        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    //Called when an object is dragged by the mouse
    private void OnMouseDrag()
    {
        transform.position = GetMouseWorldPos() + mOffset;
    }
}
