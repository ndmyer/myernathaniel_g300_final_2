﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotLoad : MonoBehaviour
{
    private Rigidbody[] allChildren;
    private Rigidbody rb;

    GameObject childGO;

    Vector3 vecFix;
    Vector3 childPos;

    private int children;
    private int mass;

    // Start is called before the first frame update
    void Awake()
    {
        // loads Bit Bot
        if (ES3.KeyExists("BotBodyBase"))
            ES3.Load<GameObject>("BotBodyBase");

        // sets vexFix to a new Vector3 respresenting position of slingshot
        vecFix = new Vector3(1.5f, 9.5f, -111.5f);

        // sets childGO to loaded in Bit Bot
        childGO = GameObject.Find("BotBodyBase");

        // Adds a BotTag script to childGo
        childGO.AddComponent<BotTag>();

        // Adds a rigidbody component to childGO
        childGO.AddComponent<Rigidbody>();

        // Sets childGo's rigidbody's gravity to false
        childGO.GetComponent<Rigidbody>().useGravity = false;

        // Sets childGo's rigidbody's Collision Detection Mode to Continuous Speculative
        childGO.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;

        //childGO.GetComponent<Collider>().isTrigger = true;

        // Parents childGO to "this" / current game object
        childGO.transform.parent = this.transform;

        // Sets childGO's position to that of vecFix
        childGO.transform.position = vecFix;

        children = childGO.transform.childCount;
        mass = 1 + children;
        rb = childGO.GetComponent<Rigidbody>();
        rb.mass = mass;
    }
}
