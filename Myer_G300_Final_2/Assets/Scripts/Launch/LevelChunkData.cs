﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LevelChunkData")]
public class LevelChunkData : ScriptableObject
{
    public enum Direction
    {
        North, East, South, West
    }

    public Vector2 chunkSize = new Vector2(300f, 300f);

    public GameObject[] levelChunks;
    public Direction entryDirection;
    public Direction exitDirection;

}
