﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceUI : MonoBehaviour
{
    private float distance;

    public Text distanceText;

    public GameObject slingShot;
    private GameObject currBot;

    private void Start()
    {
        currBot = GameObject.Find("BotBodyBase");
    }

    // Update is called once per frame
    void Update()
    {
        distance = currBot.transform.position.z - slingShot.transform.position.z;
        distance = Mathf.Max(distance, 0);
        distanceText.text = "Distance: " + distance.ToString("0");
    }
}
