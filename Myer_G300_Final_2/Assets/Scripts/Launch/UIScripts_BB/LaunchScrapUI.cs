﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LaunchScrapUI : MonoBehaviour
{
    private float distance;
    private float newScrap;

    private int oldScrap;
    private int scrap;

    public Text scrapText;

    public GameObject slingShot;
    private GameObject currBot;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene LaunchScene, LoadSceneMode single)
    {
        oldScrap = ES3.Load<int>("scrap");
    }

    private void Start()
    {
        oldScrap = ES3.Load<int>("scrap");
        //oldScrap = oldScrap - oldScrap;
        currBot = GameObject.Find("BotBodyBase");
    }

    // Update is called once per frame
    void Update()
    {
        distance = currBot.transform.position.z - slingShot.transform.position.z;
        distance = Mathf.Max(distance, 0);
        newScrap = distance/10;
        scrap = oldScrap + (int)newScrap;
        scrapText.text = "Scrap: " + scrap.ToString("0");
    }

    /*
    void OnApplicationQuit()
    {
        oldScrap = scrap;
        ES3.Save<int>("scrap", oldScrap);
    }*/

    void OnDisable()
    {
        oldScrap = scrap;
        ES3.Save<int>("scrap", oldScrap);
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}