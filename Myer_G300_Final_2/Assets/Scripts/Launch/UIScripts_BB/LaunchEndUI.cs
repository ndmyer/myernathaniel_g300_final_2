﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LaunchEndUI : MonoBehaviour
{
    public static bool PauseGame = false;
    private float distance;

    public GameObject pauseMenuUI;
    private GameObject currBot;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PauseGame)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        PauseGame = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        PauseGame = true;
    }

    public void RestartLaunch()
    {
        //Debug.Log("restart launch");
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    public void LoadBotBuilder()
    {
        //Debug.Log("load botbuilder");
        Time.timeScale = 1f;
        SceneManager.LoadScene("BotBuilderScene");
    }

    public void QuitGame()
    {
        //Debug.Log("quit game");
        Application.Quit();
    }
}